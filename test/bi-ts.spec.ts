import { expect } from "chai";
import "mocha";
import { bits, bitsSigned, flag } from "../src/index";

describe("flag", () => {
    it("flag(1, 0x02) should return 1", () => {
        const result = flag(1, 0x02);
        expect(result).to.equal(1);
    });
});
describe("bits", () => {
    it("bits(2, 1, 0x06) should return 3", () => {
        const result = bits(2, 1, 0x06);
        expect(result).to.equal(3);
    });
});
describe("bitsSigned", () => {
    it("bitsSigned(2, 1, 0x06) should return -1", () => {
        const result = bitsSigned(2, 1, 0x06);
        expect(result).to.equal(-1);
    });
});
