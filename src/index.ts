/**
 * Extracts an unsigned bit-subfield from an uint32 number.
 * The most right bit 0 is assumed to be the LSB.
 * @param from the most significant bit (MSB) of the extracted field.
 * @param to the least significant bit (LSB) of the extracted field.
 * @param uint32 an integer containing extractable bitfields.
 */
export function bits(from: number, to: number, uint32: number) {
    // tslint:disable:no-bitwise
    const mask = (1 << (from - to + 1)) - 1; // = 2^(size of bitfield)-1
    return ((uint32 >> to) & mask);
    // tslint:enable:no-bitwise
}

/**
 * Checks a flag in an integer.
 * The most right bit 0 is assumed to be the LSB.
 * @param at position of the flag.
 * @param uint32 an integer containing the flag.
 */
export function flag(at: number, uint32: number) {
    // tslint:disable-next-line:no-bitwise
    return ((uint32 >> at) & 0x01);
}

/**
 * Extracts a signed bit-subfield from an uint32 number.
 * The most right bit 0 is assumed to be the LSB.
 * @param from the position of the "signed"-bit, i.e. the most significant bit (MSB) of the extracted field.
 * @param to the least significant bit (LSB) of the extracted field.
 * @param uint32 an integer containing extractable bitfields.
 */
export function bitsSigned(from: number, to: number, uint32: number) {
    const isNegative = flag(from, uint32);
    const bitField = bits(from - 1, to, uint32);
    if (isNegative) {
        // tslint:disable-next-line:no-bitwise
        return (~bitField) + 1;
    }
    return bitField;
}
