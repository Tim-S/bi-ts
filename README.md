# bi-ts
A micro-library to slice (bit-)flags, unsigned and signed bit-fields from numbers.

### Install

If you're using Node.js, type:

```shell
npm install tim-s@bi-ts
```
or 
```shell
yarn add tim-s@bi-ts
```

### Import 
If you're using typescript:
```typescript
import { bits, bitsSigned, flag } from "@tim-s/bi-ts";
```

### Examples
```typescript
import { bits, bitsSigned, flag } from "@tim-s/bi-ts";
...
function FSPEC(octets: Uint8Array, offset: number = 0) {
    let current = offset;
    let octet: number = octets[current];
    const isSet = (bit: number) => flag(bit, octet) ? true : false;
    const fspec: boolean[] = [isSet(7), isSet(6), isSet(5), isSet(4), isSet(3), isSet(2), isSet(1)];
    while (isSet(0)) {
        octet = octets[++current];
        fspec.push(isSet(7), isSet(6), isSet(5), isSet(4), isSet(3), isSet(2), isSet(1));
    }

    return {
        end: current,
        fspec,
    };
}
...
function CalculatedTrackBarometricAltitude(octets: Uint8Array, offset: number = 0) {
    const field = new DataView(octets.buffer, offset).getUint16(0);
    const qNHCorrectionApplied = flag(15, field) ? true : false;
    const calculatedTrackBarometricAltitude =  bitsSigned(14, 0, field) * 25;
    return {
        calculatedTrackBarometricAltitude,
        end: offset + 1,
        qNHCorrectionApplied,
    };
}
...
```